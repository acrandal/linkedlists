#
# Makefile framework for CptS223
#
# Hacked up by Aaron Crandall, 2017
#  Aaron S. Crandall <acrandal@wsu.edu
#

# Variables
GPP         = g++
CFLAGS      = -g -std=c++11 -I . -Wall -Wshadow -Wconversion
GTESTFLAGS  = -pthread -lgtest
RM          = rm -f
BINNAME     = LinkedList_main
TESTNAME    = test_LinkedList_main
TESTFLAGS   = -fprofile-arcs -ftest-coverage
BINDIR      = bin
LCOVINFO    = coverage.info
COVHTMLDIR  = coverage_report
TESTSDIR    = tests

# Default is what happenes when you call make with no options
#  In this case, it requires that 'all' is completed
default: all

# All is the normal default for most Makefiles
#  In this case, it requires that Hello is completed
all: build

# Hello depends upon helloworld.cpp, then runs the command:
#  g++ -g -std=c++11 -o HelloWorld helloworld.cpp
build: $(BINNAME).cpp
	mkdir -p $(BINDIR)
	$(GPP) $(CFLAGS) -o $(BINDIR)/$(BINNAME) $(BINNAME).cpp

# Run your program
# Note: this will first execute the 'build' target to ensure a program to run
run: build
	@echo "Running the $(BINNAME) program"
	./$(BINDIR)/$(BINNAME)

# Builds the test driver system with gtest and gmock
build-test: build
	@echo "Building the test driver program"
	$(GPP) $(CFLAGS) $(TESTFLAGS) -o $(BINDIR)/$(TESTNAME) $(TESTNAME).cpp $(GTESTFLAGS)

# Test the program - first build to ensure it works
test: build build-test 
	@echo "Testing the full $(BINNAME) program suite."
	./$(BINDIR)/$(TESTNAME)

# Generate a code coverage report to show our unit testing is working
#  gcov is installed with g++ by default
#  lcov needs to be installed 'apt-get install lcov'
#  genhtml comes with lcov
coverage: build test
	@echo "Generating code coverage report on unit testing"
	gcov --relative-only -i $(TESTNAME).cpp
	lcov --no-external --capture --directory . --output-file $(LCOVINFO)
	genhtml $(LCOVINFO) --output-directory $(COVHTMLDIR)
	@echo "Run 'make clean' to remove all of these temp files"
	@echo ">>> Go into the $(COVHTMLDIR) and load index.html with browser for report"
	@echo ">>> If this is the GitLab server, there should be artifacts to look at on the right for this testing coverage job"

# Load your browser to view the coverage report
coverage-view: coverage
	@echo "Loading browser to view coverage reports"
	sensible-browser $(COVHTMLDIR)/index.html

# Do a static C++ analysis with cppcheck
lint:
	@echo "linting project with cppcheck"
	cppcheck --enable=all --error-exitcode=0 --suppress=missingIncludeSystem .

# Executes a memory leak check using the valgrind tool
memcheck: build
	@echo "Running program checks for memory leaks and some linting"
	valgrind --error-exitcode=0 --leak-check=full --trace-children=yes --show-leak-kinds=all ./$(BINDIR)/$(BINNAME)

# Runs ddd on main demo program
ddd-main: build
	@echo "Running DDD debugger on main program"
	ddd --fontsize 180 $(BINDIR)/$(BINNAME)

# Runs ddd on unit testing program
ddd-test: build
	@echo "Running DDD debugger on main program"
	ddd --fontsize 180 $(BINDIR)/$(TESTNAME)

# Runs a big demo of doing front inserts
bigdemo: build
	@echo "Running main with extra long timing demo"
	./$(BINDIR)/$(BINNAME) --timingDemo

# If you call "make clean" it will remove the built program
# Removes binaries: BINNAME, TESTNAME
# Removes code coverage temp files: *.gcno, *.gcda, *.gcov
clean veryclean:
	$(RM) $(BINDIR)/$(BINNAME) $(BINDIR)/$(TESTNAME) *.gcno *.gcda *.gcov $(LCOVINFO)
	$(RM) -r $(COVHTMLDIR) $(BINDIR)

# If you call "make starwars" it'll give you Star Wars!"
# Must have telnet installed (apt-get install telnet)
movie:
	telnet towel.blinkenlights.nl
