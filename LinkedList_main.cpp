/*
 * Main driver/demo for LinkedList class
 * 
 * Contributors:
 *   Aaron S. Crandall <acrandal@gmail.com>, 2020
 * 
 * License:
 *   See LICENSE for more details
 * 
 */

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <string.h>

#include "LinkedList.h"

void appendNodeDemo()
{
    std::cout << "Doing append node demo" << std::endl;
    LinkedList<int> listA = LinkedList<int>();          // LValue, not pointer - lives on stack
    listA.appendNode(10);
    listA.appendNode(20);
    listA.appendNode(30);
    listA.appendNode(40);
    listA.displayList();
}

void insertNodeDemo()
{
    std::cout << "Doing an insert node demo" << std::endl;
    LinkedList<int> listA = LinkedList<int>();          // LValue, not pointer - lives on stack
    listA.appendNode(10);
    listA.appendNode(20);
    listA.appendNode(40);
    listA.insertNode(30);
    listA.displayList();
}

void deleteNodeDemo()
{
    std::cout << "Doing a delete node demo" << std::endl;
    LinkedList<int> listA = LinkedList<int>();          // LValue, not pointer - lives on stack
    listA.appendNode(10);
    listA.appendNode(20);
    listA.appendNode(30);
    listA.appendNode(40);
    listA.deleteNode(30);
    listA.displayList();
}


void initializerListConstructorDemo()
{
    std::cout << "Doing initializer_list constructor list demo" << std::endl;
    LinkedList<int> listB = LinkedList<int>( {2, 3, 5, 7} ); // Initializer list constructor
    listB.displayList();
}


void insertHeadTimingDemo(int value_count)
{
    int dot_step = value_count / 40;
    std::cout << "Doing insert head timing demo with " << value_count << " items." << std::endl;
    std::vector<int> vec;

    std::cout << "LinkedList head insert time: " << std::flush;
    LinkedList<int> lst = LinkedList<int>();
    auto clk_start_lst = std::chrono::high_resolution_clock::now();
    for(int i = value_count; i > 0; i--) {
        lst.insertNode(i);
        if( i % dot_step == 0 )
          { std::cout << "." << std::flush; }
    }
    auto clk_end_lst = std::chrono::high_resolution_clock::now();
    auto lst_ns = clk_end_lst.time_since_epoch().count() - clk_start_lst.time_since_epoch().count();
    std::cout << lst_ns << " (ns)" << std::endl;

    std::cout << "Vector head insert time:     " << std::flush;
    auto itr = vec.begin();                 // Get iterator to start of vector
    auto clk_start_vec = std::chrono::high_resolution_clock::now();
    for(int i = value_count; i > 0; i--) {
        vec.insert(itr, i);
        itr = vec.begin();                  // Need a new copy each time
        if( i % dot_step == 0 )
          { std::cout << "." << std::flush; }
    }
    auto clk_end_vec = std::chrono::high_resolution_clock::now();
    auto vec_ns = clk_end_vec.time_since_epoch().count() - clk_start_vec.time_since_epoch().count();
    std::cout << vec_ns << " (ns)";

    float ratio = (float)vec_ns / (float)lst_ns;
    std::cout << std::fixed <<  " -- " << std::setprecision(2) << ratio << " times slower." << std::endl;
}

int main(int argc, char* argv[]) {
    std::cout << "Starting Linked List Demo" << std::endl;

    bool do_headTimingDemo = false;
    for( int i = 0; i < argc; i++ ) {
        if( strcmp(argv[i], "--timingDemo") == 0 ) {
            do_headTimingDemo = true;
        }
    }

    if( do_headTimingDemo ) {
        insertHeadTimingDemo(250000);
        insertHeadTimingDemo(500000);
    } else {
        appendNodeDemo();
        insertNodeDemo();
        deleteNodeDemo();
    }
}

