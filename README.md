# Linked List Lecture Implementation

Singly linked list in C++  
Both Linked List and List Nodes are C++ templated classes  
 
#### Contributors:  
    Tony Gaddis  
    Starting Out with C++ from Control Structures to Objects (9th Edition)  
    ISBN-13: 978-0134544847  
    Copyright 2017  

#### Additional changes, notably testing and continuous integration components:  
    Aaron S. Crandall <acrandal@gmail.com>, 2020

#### Copyright:  
    Gaddis' work falls under their code license for their textbook
    Additional work under Creative Commons:  
        Attribution-NonCommercial-ShareAlike 4.0 International  
        http://creativecommons.org/licenses/by-nc-sa/4.0/  

#### Summary:
    A linked list implementation, but not a full STL API style.  
    This work is designed to be illustrative more than efficient  

#### Dependencies:
    The main LinkedList has few dependencies beyond core C++11 or higher  
    Testing harness done with googletest: https://github.com/google/googletest  
    Continuous Integration designed for GitLab CI/CD in .gitlab-ci.yml  


