/*
 *  Copyright 2019 - See README for details
 *
 *  Contributors:
 *   Aaron S. Crandall <acrandal@gmail.com>, 2020
 */

#ifndef __CORE_TESTS_H
#define __CORE_TESTS_H

#include <vector>               // C++ STL vector type

// googletest C++ testing harnesses can be found here:
//  https://github.com/google/googletest
#include <gtest/gtest.h>        // Google's C++ testing harness
#include <gmock/gmock.h>        // Google's object mocking library

#include "LinkedList.h"         // Include library in project for headers

using namespace testing;        // gtest lives in the testing:: namespace

// **************** Start of LinkedList API tests ******************** //
TEST(Core_LinkedList, Empty)
{
    LinkedList<int> listA = LinkedList<int>();
    ASSERT_EQ(true, listA.empty());
}

TEST(Core_LinkedList, Size_0)
{
    LinkedList<int> listA = LinkedList<int>();
    int target_size = 0;
    ASSERT_EQ(target_size, listA.size());
}


// Tests after appendNode functional ************************************ //

TEST(Core_LinkedList, NotEmpty)
{
    LinkedList<int> listA = LinkedList<int>();
    listA.appendNode(50);
    ASSERT_EQ(false, listA.empty());
}

TEST(Core_LinkedList, Contains)
{
    LinkedList<int> listA = LinkedList<int>();
    for( int i = 0; i < 30; i++ )
        { listA.appendNode(i); }
    ASSERT_EQ(true, listA.contains(20));
}

TEST(Core_LinkedList, NotContains)
{
    LinkedList<int> listA = LinkedList<int>();
    for( int i = 0; i < 30; i++ ) 
        { listA.appendNode(i); }
    ASSERT_EQ(false, listA.contains(50));
}

TEST(Core_LinkedList, Size_30)
{
    LinkedList<int> listA = LinkedList<int>();
    int target_size = 30;
    for( int i = 0; i < target_size; i++ ) 
        { listA.appendNode(i); }
    ASSERT_EQ(target_size, listA.size());
}

// Tests after initalizer constructor functional ************************ //

TEST(Core_LinkedList, Initializer_List_5_Not_Empty)
{
    LinkedList<int> listA = LinkedList<int>({3, 7, 1, 5, 10});
    ASSERT_EQ(false, listA.empty());
}

TEST(Core_LinkedList, Initializer_List_Size_5)
{
    int target_size = 5;
    LinkedList<int> listA = LinkedList<int>({3, 7, 1, 5, 10});
    ASSERT_EQ(target_size, listA.size());
}

TEST(Core_LinkedList, Initializer_List_Data_Vals)
{
    std::vector<int> target = std::vector<int>({3, 7, 1, 5, 10});
    LinkedList<int> listA = LinkedList<int>({3, 7, 1, 5, 10});
    std::vector<int> & data = listA.toVector();
    bool result = std::equal(target.begin(), target.end(), data.begin());
    ASSERT_EQ(true, result);
}


// Tests after deleteNode functional ************************************ //

TEST(Core_LinkedList, Add_Remove_Empty)
{
    LinkedList<int> listA = LinkedList<int>();
    listA.appendNode(30);
    listA.deleteNode(30);
    ASSERT_EQ(true, listA.empty());
}

TEST(Core_LinkedList, Add_Fail_Remove_Empty)
{
    LinkedList<int> listA = LinkedList<int>();
    listA.appendNode(30);
    listA.deleteNode(40);
    ASSERT_EQ(false, listA.empty());
}

TEST(Core_LinkedList, Add_Fail_Remove_Empty_Size_1)
{
    LinkedList<int> listA = LinkedList<int>();
    listA.appendNode(30);
    listA.deleteNode(40);
    ASSERT_EQ(1, listA.size());
}

TEST(Core_LinkedList, Remove_Tail)
{
    LinkedList<int> listA = LinkedList<int>();
    int max_size = 30;
    int target_size = 29;
    for( int i = 0; i < max_size; i++ ) 
        { listA.appendNode(i); }
    listA.deleteNode(29);
    ASSERT_EQ(target_size, listA.size());
}

TEST(Core_LinkedList, Remove_Head)
{
    LinkedList<int> listA = LinkedList<int>();
    int max_size = 30;
    int target_size = 29;
    for( int i = 0; i < max_size; i++ ) 
        { listA.appendNode(i); }
    listA.deleteNode(0);
    ASSERT_EQ(target_size, listA.size());
}

TEST(Core_LinkedList, Remove_Middle)
{
    LinkedList<int> listA = LinkedList<int>();
    int max_size = 30;
    int target_size = 29;
    for( int i = 0; i < max_size; i++ ) 
        { listA.appendNode(i); }
    listA.deleteNode(15);
    ASSERT_EQ(target_size, listA.size());
}

TEST(Core_LinkedList, Remove_Head_Data_Vals)
{
    std::vector<int> target = std::vector<int>({7, 1, 5, 10});
    LinkedList<int> listA = LinkedList<int>({3, 7, 1, 5, 10});
    listA.deleteNode(3);
    std::vector<int> & data = listA.toVector();
    bool result = std::equal(target.begin(), target.end(), data.begin());
    ASSERT_EQ(true, result);
}

TEST(Core_LinkedList, Remove_Tail_Data_Vals)
{
    std::vector<int> target = std::vector<int>({3, 7, 1, 5});
    LinkedList<int> listA = LinkedList<int>({3, 7, 1, 5, 10});
    listA.deleteNode(10);
    std::vector<int> & data = listA.toVector();
    bool result = std::equal(target.begin(), target.end(), data.begin());
    ASSERT_EQ(true, result);
}

TEST(Core_LinkedList, Size_Grow_30_Shrink_to_20)
{
    LinkedList<int> listA = LinkedList<int>();
    int max_size = 30;
    int target_size = 20;
    int remove_node_count = 10;
    for( int i = 0; i < max_size; i++ ) 
        { listA.appendNode(i); }
    for( int i = 0; i < remove_node_count; i++ )
        { listA.deleteNode(i); }
    ASSERT_EQ(target_size, listA.size());
}

TEST(Core_LinkedList, Remove_Middle_Data_Vals)
{
    std::vector<int> target = std::vector<int>({3, 1, 10});
    LinkedList<int> listA = LinkedList<int>({3, 7, 1, 5, 10});
    listA.deleteNode(5);
    listA.deleteNode(7);
    std::vector<int> & data = listA.toVector();
    bool result = std::equal(target.begin(), target.end(), data.begin());
    ASSERT_EQ(true, result);
}

TEST(Core_LinkedList, NotContains_After_Remove)
{
    LinkedList<int> listA = LinkedList<int>();
    for( int i = 0; i < 30; i++ ) 
        { listA.appendNode(i); }
    listA.deleteNode(10);
    ASSERT_EQ(false, listA.contains(10));
}


#endif      // __CORE_TESTS_H
