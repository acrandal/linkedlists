/*
 *  Copyright 2019 - See README for details
 *
 *  Contributors:
 *   Aaron S. Crandall <acrandal@gmail.com>, 2020
 */

#ifndef __BIG5_TESTS_H
#define __BIG5_TESTS_H

#include <vector>               // C++ STL vector type

// googletest C++ testing harnesses can be found here:
//  https://github.com/google/googletest
#include <gtest/gtest.h>        // Google's C++ testing harness
#include <gmock/gmock.h>        // Google's object mocking library

#include "LinkedList.h"         // Include library in project for headers

using namespace testing;        // gtest lives in the testing:: namespace

// **************** Start of LinkedList Big 5 tests ******************** //
TEST(Big5_LinkedList, CopyConstructor)
{
    std::vector<int> targetVec({1,2,3,4,5});    // Vector to test against
    LinkedList<int> firstList({1,2,3,4,5});
    LinkedList<int> copiedList{ firstList };    // Executes copy constructor
    std::vector<int> foundVec = copiedList.toVector();
    ASSERT_EQ(true, targetVec == foundVec);
}

TEST(Big5_LinkedList, MoveConstructor)
{
    std::vector<int> targetVec({1,2,3,4,5});    // Vector to test against
    LinkedList<int> firstList({1,2,3,4,5});
    LinkedList<int> movedList = std::move(firstList); // Executes move constructor
    std::vector<int> foundVec = movedList.toVector();
    ASSERT_EQ(0, firstList.size());
    ASSERT_EQ(5, movedList.size());
    ASSERT_EQ(true, targetVec == foundVec);
    // *should* get the addresses of the firstList nodes and check that they're
    //  identical in movedList instead of copies of the values
}

TEST(Big5_LinkedList, CopyAssignmentOperator)
{
    std::vector<int> targetVec({1,2,3,4,5});    // Vector to test against
    LinkedList<int> firstList({1,2,3,4,5});
    LinkedList<int> copiedList;

    copiedList = firstList;                     // Executed copy assignment operator

    std::vector<int> foundVec = copiedList.toVector();
    ASSERT_EQ(5, firstList.size());
    ASSERT_EQ(5, copiedList.size());
    ASSERT_EQ(true, targetVec == foundVec);
    // *should* get the addresses of the firstList nodes and check that they're
    //  identical in movedList instead of copies of the values
}

TEST(Big5_LinkedList, MoveAssignmentOperator)
{
    std::vector<int> targetVec({1,2,3,4,5});    // Vector to test against
    LinkedList<int> firstList({1,2,3,4,5});

    LinkedList<int> movedList({7,8});           // Create list to move to
    movedList = LinkedList<int>{1,2,3,4,5};

    std::vector<int> foundVec = movedList.toVector();
    ASSERT_EQ(5, movedList.size());
    ASSERT_EQ(true, targetVec == foundVec);
}



#endif      // __BIG5_TESTS_H
