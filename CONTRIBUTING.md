Contributing:

Bugfixes, updates, and comments may be sent via pull request or to Aaron Crandall <acrandal@gmail.com>.
