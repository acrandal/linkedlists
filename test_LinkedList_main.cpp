/*
 *  Test suite for Linked List class
 *
 *  Copyright 2019 - See README for details
 *
 *  Contributors:
 *   Aaron S. Crandall <acrandal@wsu.edu>
 */

// googletest C++ testing harnesses can be found here:
//  https://github.com/google/googletest
#include <gtest/gtest.h>        // Google C++ testing harness
#include <gmock/gmock.h>        // Google object mocking library

#include "tests/test_LinkedList_bigFive.h"
#include "tests/test_LinkedList_core.h"

// Main only sets up the Google gtest system and runs the tests
// Any Functions called "TEST" will be run
// These tests are all kept in the tests/ directory
//  -- See the #include "tests/*" above
int main(int argc, char* argv[])
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
