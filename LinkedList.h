/*
 *  Linked List
 *   Singly linked list in C++
 *   Both Linked List and List Nodes are C++ templated classes
 * 
 *  Contributors:
 *      Tony Gaddis 
 *      Starting Out with C++ from Control Structures to Objects (9th Edition)
 *      ISBN-13: 978-0134544847
 *      Copyright 2017
 * 
 *  Additional changes:
 *      Aaron S. Crandall <acrandal@gmail.com> 2020
 * 
 *  Copyright:
 *   Gaddis' work falls under their code license
 *   Additional work under Creative Commons
 *    Attribution-NonCommercial-ShareAlike 4.0 International
 *    http://creativecommons.org/licenses/by-nc-sa/4.0/
 * 
 *  Summary:
 *   A linked list implementation, but not a full STL API style.
 *   This work is designed to be illustrative more than efficient
 * 
 */

#ifndef __LINKEDLIST_H
#define __LINKEDLIST_H

#include <vector>
#include <initializer_list>


//*********************************************
// LinkedList class
//*********************************************
template <class T>
class LinkedList
{

private:
    //*********************************************
    // The ListNode class creates a type used to store a node of
    //  the linked list.
    // This class is *private* to the LinkedList which guarantees
    //  no other classes can create ListNodes.
    // This is a form of information hiding and ensures the
    //  LinkedList implementation details are not used by other classes.
    // This becomes very important in packages for coupling and cohesion.
    //*********************************************
    template <class T2>
    class ListNode
    {
    public:
        T2 value;                    // Node value
        ListNode<T2> *next;          // Pointer to the next node

        explicit ListNode (T2 nodeValue) {    // Constructor
            value = nodeValue;
            next = nullptr;
        }
    };


private:
    ListNode<T> *head;              // List head pointer



public:
    explicit LinkedList() {         // Constructor
        head = nullptr;
    }

    ~LinkedList();                  // Destructor

    // Linked list operations
    void appendNode(T);
    void insertNode(T);
    void deleteNode(T);
    void displayList() const;
    bool empty() const;
    bool contains(T searchValue);
    int size() const;
    std::vector<T> & toVector() const;


    // ************************ Big 5 Constructors ************************* //
    // Copy constructor ****
    LinkedList(const LinkedList<T> &other)
    {
        // std::cout << "Copy constructor" << std::endl;
        head = nullptr;                 // Always init to be safe
        if (this != &other) {
            ListNode<T>* curr = other.head;
            while( curr != nullptr ) {
                appendNode(curr->value);
                curr = curr->next;
            }
        }
    }

    // Move constructor ****
    LinkedList(LinkedList<T> &&other)
    {
        // std::cout << "Move constructor" << std::endl;
        head = nullptr;                 // Always init to be safe
        if( this != &other ) {
            head = other.head;          // Grab their list head pointer
            other.head = nullptr;       // Other no longer has the list
            // If you also had other members like tail or size, grab them too
        }
    }

    // Initializer list constructor ****
    explicit LinkedList(std::initializer_list<T> values)
    {
        head = nullptr;
        for (auto item : values)    // Foreach item in values
            { appendNode(item); }   // Add the item to list
    }

    // Copy assignment operator ****
    virtual LinkedList<T> &operator=(const LinkedList<T> &other)
    {
        // std::cout << "Copy assignment operator" << std::endl;
        if (this != &other) {                   // Can't copy ourselves?
            while( this->size() > 0 ) {         // Remove all of our current nodes
                deleteNode( this->head->value );
            }
            ListNode<T>* curr = other.head;     // Start at their head
            while( curr != nullptr ) {          // While more data to copy
                appendNode(curr->value);        // Add that node to ourselves
                curr = curr->next;              // Move down other's list
            }
        }
        return *this;                           // Operators return pointers
    }

    // Move assignment operator ****
    virtual LinkedList<T> &operator=(LinkedList<T> &&other)
    {
        // std::cout << "Move assignment operator" << std::endl;
        if (this != &other) {                   // Can't copy ourselves?
            while( this->size() > 0 ) {         // Remove all of our current nodes
                deleteNode( this->head->value );
            }
            this->head = other.head;
            other.head = nullptr;
            // If you also had other members like tail or size, grab them too
        }
        return *this;                           // Operators return pointers
    }

};      // ** End of LinkedList class API definition *************************


//**************************************************
// appendNode appends a node containing the value
// passed into newValue, to the end of the list.
//**************************************************
template <class T>
void LinkedList<T>::appendNode(T newValue)
{
    ListNode<T> *newNode; // To point to a new node
    ListNode<T> *nodePtr; // To move through the list

    // Allocate a new node and store newValue there.
    newNode = new ListNode<T>(newValue);

    // If there are no nodes in the list make newNode the first node.
    if (head == nullptr) {
        head = newNode;
    }
    else                        // Otherwise, insert newNode at end.
    {
        nodePtr = head;         // Initialize nodePtr to head of list.
        while (nodePtr->next) { // Find the last node in the list.
            nodePtr = nodePtr->next;
        }
        nodePtr->next = newNode; // Insert newNode as the last node.
    }
}

//***************************************************
// displayList shows the value stored in each node
// of the linked list pointed to by head.
//***************************************************
template <class T>
void LinkedList<T>::displayList() const
{
    ListNode<T> *nodePtr; // To move through the list

    // Position nodePtr at the head of the list.
    nodePtr = head;

    // While nodePtr points to a node, traverse the list.
    //  Loop terminates if nodePtr == nullptr
    while (nodePtr != nullptr)
    {
        std::cout << nodePtr->value << std::endl;   // Display the value in this node.
        nodePtr = nodePtr->next;                    // Move to the next node.
    }
}

//**************************************************
// The insertNode function inserts a node with newValue in sorted order.
//  This is functionally the same as Insertion Sort.
//**************************************************
template <class T>
void LinkedList<T>::insertNode(T newValue)
{
    ListNode<T> *newNode;                   // A new node
    ListNode<T> *nodePtr;                   // To traverse the list
    ListNode<T> *previousNode = nullptr;    // The previous node

    // Allocate a new node and store newValue there.
    newNode = new ListNode<T>(newValue);

    // If there are no nodes in the list make newNode the first node.
    if (head == nullptr) {
        head = newNode;
        newNode->next = nullptr;
    }
    else                                    // Otherwise, insert newNode.
    {
        nodePtr = head;         // Position nodePtr at the head of list.
        previousNode = nullptr; // Initialize previousNode to nullptr.

        // Skip all nodes whose value is less than newValue.
        //  Remember: && is a short circuit logic operator
        //  This means the right side is only done if the left is true
        while (nodePtr != nullptr && nodePtr->value < newValue)
        {
            previousNode = nodePtr;
            nodePtr = nodePtr->next;
        }

        // If the new node is to be the 1st in the list,
        //  insert it before all other nodes.
        if (previousNode == nullptr)
        {
            head = newNode;
            newNode->next = nodePtr;
        }
        else // Otherwise insert after the previous node.
        {
            previousNode->next = newNode;
            newNode->next = nodePtr;
        }
    }
}

//******************************************************
// The deleteNode function searches for a node
//  with searchValue as its value. The node, if found,
//  is deleted from the list and from memory.
//******************************************************
template <class T>
void LinkedList<T>::deleteNode(T searchValue)
{
    ListNode<T> *nodePtr;               // To traverse the list
    ListNode<T> *previousNode;          // To point to the previous node

    // If the list is empty, do nothing.
    if (head == nullptr)
        { return; }

    // Determine if the first node is the one.
    if (head->value == searchValue)
    {
        nodePtr = head->next;
        delete head;
        head = nodePtr;
    }
    else
    {
        nodePtr = head;             // Initialize nodePtr to head of list

        // Skip all nodes whose value member is not equal to num.
        //  Remember: && is a short circuit logic operator
        //  This means the right side is only done if the left is true
        while (nodePtr != nullptr && nodePtr->value != searchValue)
        {
            previousNode = nodePtr;
            nodePtr = nodePtr->next;
        }

        // If nodePtr is not at the end of the list,
        //  link the previous node to the node after
        //  nodePtr, then delete nodePtr.
        if(nodePtr != nullptr)
        {
            previousNode->next = nodePtr->next;
            delete nodePtr;
        }
    }
}

//**************************************************
// empty 
// Returns true if list has no nodes (is empty), else false
//**************************************************
template <class T>
bool LinkedList<T>::empty() const
{
    if(!head) {
        return true;
    }
    return false;

}

//**************************************************
// contains
// Returns true if list has a node with the search value
//**************************************************
template <class T>
bool LinkedList<T>::contains(T searchValue)
{
    ListNode<T> *nodePtr;               // To traverse the list

    // If the list is empty, do nothing.
    if (head == nullptr)
        { return false; }

    // Determine if the first node is the one.
    if (head->value == searchValue)
    {
        nodePtr = head->next;
        delete head;
        head = nodePtr;
    }
    else
    {
        nodePtr = head;             // Initialize nodePtr to head of list

        // Skip all nodes whose value member is not equal to num.
        while (nodePtr != nullptr && nodePtr->value != searchValue)
            { nodePtr = nodePtr->next; }

        // If nodePtr is not at the end of the list,
        //  then we MUST have found the searchValue
        if(nodePtr)
            { return true; }
    }
    return false;
}

//***************************************************
// toVector
//  returns a vector of the data values in the list
//***************************************************
template <class T>
std::vector<T> & LinkedList<T>::toVector() const
{
    ListNode<T> *nodePtr;               // To move through the list
    std::vector<T> * data = new std::vector<T>();  // On heap to be returned as lvalue

    nodePtr = head;                     // Position nodePtr at the head of the list.

    // While nodePtr points to a node, traverse the list.
    //  Loop terminates if nodePtr == nullptr
    while (nodePtr != nullptr)
    {
        data->push_back(nodePtr->value);     // Append a list value to vector
        nodePtr = nodePtr->next;            // Move to the next node.
    }
    return *data;
}

//***************************************************
// size
//  returns the quantity of nodes in the list
//***************************************************
template <class T>
int LinkedList<T>::size() const
{
    ListNode<T> *nodePtr;               // To move through the list
    int size = 0;                       // Tracking quantity of nodes

    nodePtr = head;                     // Position nodePtr at the head of the list.

    // While nodePtr points to a node, traverse the list.
    //  Loop terminates if nodePtr == nullptr
    while (nodePtr != nullptr)
    {
        size++;                         // Count node
        nodePtr = nodePtr->next;        // Move to the next node.
    }
    return size;
}

//**************************************************
// Destructor
// This function deletes every node in the list.
//**************************************************
template <class T>
LinkedList<T>::~LinkedList()
{
    ListNode<T> *nodePtr;       // To traverse the list
    ListNode<T> *nextNode;      // To point to the next node

    nodePtr = head;             // Position nodePtr at the head of the list.

    // While nodePtr is not at the end of the list...
    while (nodePtr != nullptr)
    {
        nextNode = nodePtr->next;       // Save a pointer to the next node.

        delete nodePtr;                 // Delete the current node.
        nodePtr = nextNode;             // Position nodePtr at the next node.
    }
}

#endif      // __LINKEDLIST_H